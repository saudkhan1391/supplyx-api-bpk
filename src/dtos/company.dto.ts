import { Attachment, CompanyAttachment } from '@/interfaces/image.interface';
import { IsEmail, IsString, IsOptional, IsEnum, IsNumber, IsNotEmpty, IsArray } from 'class-validator';

export class CreateCompanyDto {
  @IsOptional()
  id: number;

  @IsString()
  @IsOptional()
  public banner: string;

  @IsString()
  @IsOptional()
  public about: string;

  @IsString()
  @IsOptional()
  public name: string;

  @IsOptional()
  @IsString()
  public street: string;

  @IsString()
  @IsOptional()
  public postalCode: string;


  @IsString()
  @IsNotEmpty()
  public whyChooseUs: string;

  @IsArray()
  @IsOptional()
  public images: CompanyAttachment[];

  @IsArray()
  @IsOptional()
  public aboutImage: CompanyAttachment[];

  @IsArray()
  @IsOptional()
  public bannerImage: CompanyAttachment[];

  @IsArray()
  @IsOptional()
  public chooseusImage: CompanyAttachment[];

  @IsArray()
  @IsOptional()
  public profileImage: string[];

  @IsNotEmpty()
  public email: string;

  @IsString()
  @IsNotEmpty()
  public phone: string;

  @IsString()
  @IsNotEmpty()
  public mobile: string;

  @IsString()
  @IsNotEmpty()
  public website: string;

  @IsString()
  @IsNotEmpty()
  public youtubeChannel: string;
}
