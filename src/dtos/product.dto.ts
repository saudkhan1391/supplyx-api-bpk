import { Catalog } from '@/interfaces/catalog.interface';
import Category from '@/interfaces/category.interface';
import { Attachment } from '@/interfaces/image.interface';
import { Product } from '@/interfaces/product.interface';
import { IsString, IsOptional, IsEnum, IsNumber, IsNotEmpty, IsArray } from 'class-validator';

export class CreateProductDto implements Product{
  @IsOptional()
  id: number;

  @IsOptional()
  userId: number;

  @IsString()
  @IsNotEmpty()
  public productName: string;

  @IsOptional()
  public category: Category;

  @IsString()
  @IsNotEmpty()
  public brand: string;

  @IsString()
  @IsNotEmpty()
  public manufacturer: string;

  @IsString()
  @IsNotEmpty()
  public partNo: string;

  @IsString()
  public hsCode: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public description: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public price: string;

  @IsOptional()
  public minimumQuantity: number;

  @IsOptional()
  public availableQuantity: number;

  @IsOptional()
  public shipinfo: string;

  @IsOptional()
  @IsArray()
  specifications: Attachment[];

  @IsOptional()
  @IsArray()
  images: Attachment[];

  @IsEnum(["draft", "active", "inactive", "deleted"])
  public status: string;

  @IsOptional()
  public catalog: Catalog;

}
