import { Address } from '@/interfaces/address.interface';
import { ApartmentType } from '@/interfaces/apartment-type.interface';
import { Building } from '@/interfaces/building.interface';
import { ProjectAttachment, ProjectImage } from '@/interfaces/image.interface';
import { IsString, IsOptional, IsNotEmpty, IsNumber, IsArray } from 'class-validator';

export class ProjectDto {
  @IsOptional()
  id: number;

  @IsNumber()
  @IsOptional()
  userId: number;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public name: string;

  @IsString()
  @IsNotEmpty()
  public type: string;

  @IsOptional()
  description: string;

  @IsOptional()
  image: ProjectImage;

  @IsOptional()
  status: string;

  @IsOptional()
  address: Address;

  @IsOptional()
  buildings: Building[];

  @IsOptional()
  @IsArray()
  apartmentTypes: ApartmentType[];

  @IsOptional()
  @IsArray()
  specifications: ProjectAttachment[];

}
