import { IsString, IsOptional, IsNotEmpty, IsNumber, IsArray } from 'class-validator';

export class CatalogProductMappingRequestDto {

  @IsNumber()
  id: number;

  @IsArray()
  @IsNotEmpty()
  public productIds: number[];

 
}
