import { IsNumber, IsOptional } from "class-validator";

export class PaginationOptionsDto {
    @IsNumber()
    page: number;

    @IsNumber()
    take: number;

    @IsOptional()
    status: string;

    @IsOptional()
    filter: string;

    @IsOptional()
    filterBy: string;
}