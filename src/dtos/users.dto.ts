import { UserImage } from '@/interfaces/image.interface';
import { IsEmail, IsString, IsOptional, IsEnum, IsNumber, IsNotEmpty, IsArray } from 'class-validator';

export class CreateUserDto {
  @IsOptional()
  id: number

  @IsEmail()
  @IsNotEmpty()
  @IsOptional()
  public email: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public password: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public confirmPassword: string;

  @IsString()
  @IsNotEmpty()
  public firstName: string;

  @IsString()
  @IsNotEmpty()
  public lastName: string;

  @IsEnum({'buyer':'1','seller':'2'})
  public roleType: string;

  @IsString()
  @IsOptional()
  public companyName: string;

  @IsString()
  @IsOptional()
  public country: string;

  @IsNumber()
  @IsOptional()
  public zipCode: number;

  @IsString()
  @IsOptional()
  public street: string;

  @IsString()
  @IsOptional()
  public state: string;

  @IsString()
  @IsOptional()
  public mobile: string;

  @IsString()
  @IsOptional()
  public phone: string;

  @IsString()
  @IsOptional()
  public phoneCode: string;

  @IsOptional()
  @IsArray()
  images:UserImage[];
}
