import { Product } from '@/interfaces/product.interface';
import { IsString, IsOptional, IsNotEmpty } from 'class-validator';

export class CreateCatalogDto {

  @IsOptional()
  id: number;

  @IsString()
  @IsNotEmpty()
  public catalogName: string;

  @IsString()
  @IsNotEmpty()
  public description: string;

  @IsOptional()
  userId: number;

  @IsOptional()
  productCount: number;

  @IsOptional()
  products: Product[]
 
}
