import { IsString, IsOptional, IsNotEmpty } from 'class-validator';

export class CategoryDto {

  @IsOptional()
  id: number;

  @IsString()
  @IsNotEmpty()
  public name: string;
 
}
