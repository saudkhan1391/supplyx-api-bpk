import { ApartmentType } from '@/interfaces/apartment-type.interface';
import { IsString, IsOptional, IsNotEmpty } from 'class-validator';

export class AreaDto {
  @IsOptional()
  id: number;

  @IsOptional()
  apartmentType: ApartmentType;

  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsOptional()
  count: number;
}
