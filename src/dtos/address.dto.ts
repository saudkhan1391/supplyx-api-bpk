import { IsString, IsOptional, IsNotEmpty } from 'class-validator';

export class AddressDto {
  @IsOptional()
  id: number;

  @IsString()
  @IsNotEmpty()
  public street: string;

  @IsString()
  @IsOptional()
  public city: string;

  @IsString()
  @IsNotEmpty()
  zipCode: string;

  @IsString()
  @IsNotEmpty()
  state: string;

  @IsString()
  @IsNotEmpty()
  country: string;
}
