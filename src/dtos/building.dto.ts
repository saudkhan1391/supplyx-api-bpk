import { Floor } from '@/interfaces/floor.interface';
import { Project } from '@/interfaces/project.interface';
import { IsString, IsOptional, IsNotEmpty, IsArray } from 'class-validator';

export class BuildingDto {
  @IsOptional()
  id: number;

  @IsOptional()
  project: Project;

  @IsString()
  @IsNotEmpty()
  public name: string;
 
  @IsOptional()
  floorCount: string;

  @IsArray()
  floors: Floor[]
}
