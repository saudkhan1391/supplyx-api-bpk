import { Area } from '@/interfaces/area.interface';
import { Project } from '@/interfaces/project.interface';
import { IsString, IsOptional, IsNotEmpty, IsNumber, IsArray } from 'class-validator';

export class ApartmentTypeDto {
  
  @IsOptional()
  id: number;

  @IsOptional()
  project: Project;

  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsNumber()
  size: number;

  @IsArray()
  areas: Area[];

}

