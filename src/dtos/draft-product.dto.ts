import { Catalog } from '@/interfaces/catalog.interface';
import Category from '@/interfaces/category.interface';
import { Attachment } from '@/interfaces/image.interface';
import { Product } from '@/interfaces/product.interface';
import { IsString, IsOptional, IsEnum, IsNumber, IsNotEmpty } from 'class-validator';

export class CreateDraftProductDto implements Product{
  @IsOptional()
  id: number;

  @IsOptional()
  userId: number;

  @IsString()
  @IsNotEmpty()
  public productName: string;

  @IsOptional()
  public category: Category;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public brand: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public manufacturer: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public partNo: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public info: string;

  @IsString()
  @IsOptional()
  public hsCode: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public description: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public price: string;

  @IsOptional()
  public minimumQuantity: number;

  @IsOptional()
  public availableQuantity: number;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public shipinfo: string;

  @IsOptional()
  specifications: Attachment[];

  @IsOptional()
  images: Attachment[];

  @IsEnum(["draft", "active", "inactive", "deleted"])
  public status: string;

  @IsOptional()
  public catalog: Catalog;

}
