import { RequestWithUser } from "@/interfaces/auth.interface";
import { User } from "@/interfaces/users.interface";
import authMiddleware from "@/middlewares/auth.middleware";
import imgUploadOptions from "@/middlewares/file-handler.middleware";
import { UploadedFile } from "@/models/file-uploaded.model";
import AwsS3Service from "@/services/aws.service";
import multer, { FileFilterCallback, memoryStorage, Multer } from "multer";
import { Controller, Delete, HttpCode, Param, Post, Req, UseBefore } from "routing-controllers";

@Controller()
export class FileUploadController {

    public awsS3Service: AwsS3Service = new AwsS3Service();

    @Post('/upload')
    @UseBefore(multer(imgUploadOptions).fields([
        { name: "upload" }
    ]))
    @HttpCode(201)
    async uploadFiles(@Req() req: RequestWithUser) {
        const uploadedFiles: UploadedFile | UploadedFile[] = await this.awsS3Service.upload(req.files['upload'])
        return { data: uploadedFiles, message: 'aws S3 upload' };
    }

    @Delete('/upload/:key')
    @HttpCode(200)
    async deleteFile(@Req() req: RequestWithUser, @Param('key') key: string) {
        const deleteResponse: string = await this.awsS3Service.delete(key);
        return { data: deleteResponse, message: 'delete object' };
    }

}