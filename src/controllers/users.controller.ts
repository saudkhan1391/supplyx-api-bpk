import { RequestWithUser } from '@interfaces/auth.interface';
import { Controller, Param, Body, Get, Post, Put, Delete, HttpCode, UseBefore, Req } from 'routing-controllers';
import { OpenAPI } from 'routing-controllers-openapi';
import { CreateUserDto } from '@dtos/users.dto';
import { User } from '@interfaces/users.interface';
import userService from '@services/users.service';
import { validationMiddleware } from '@middlewares/validation.middleware';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import authMiddleware from '@/middlewares/auth.middleware';

@Controller()
export class UsersController {
  public userService = new userService();

  @Post('/users/all')
  @OpenAPI({ summary: 'Return a list of users' })
  async getUsers(@Body() paginationOptions: PaginationOptions) {
    const findAllUsersData: Pagination<User> = await this.userService.findAllUser(paginationOptions);
    return { data: findAllUsersData, message: 'findAll' };
  }

  @Get('/users/:id')
  @OpenAPI({ summary: 'Return find a user' })
  async getUserById(@Param('id') userId: number) {
    const findOneUserData: User = await this.userService.findUserById(userId);
    return { data: findOneUserData, message: 'findOne' };
  }

  @Post('/users')
  @HttpCode(201)
  @UseBefore(validationMiddleware(CreateUserDto, 'body'))
  @OpenAPI({ summary: 'Create a new user' })
  async createUser(@Body() userData: CreateUserDto) {
    const createUserData: User = await this.userService.createUser(userData);
    return { data: createUserData, message: 'created' };
  }

  @Post('/uusers')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateUserDto, 'body'))
  @OpenAPI({ summary: 'Update a user' })
  async updateUser(@Req() req: RequestWithUser, @Body() userData: CreateUserDto) {
    const users: User = req.user;
    console.log('User',users);
    const updateUserData: User = await this.userService.updateUser(users.id, userData);
    return { data: updateUserData, message: 'updated' };
  }

  @Delete('/users/:id')
  @OpenAPI({ summary: 'Delete a user' })
  async deleteUser(@Param('id') userId: number) {
    const deleteUserData: User = await this.userService.deleteUser(userId);
    return { data: deleteUserData, message: 'deleted' };
  }
}
