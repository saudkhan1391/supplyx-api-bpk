import { Controller, Req, Body, Post, Get, Param, Put, UseBefore, HttpCode, Delete } from 'routing-controllers';
import { RequestWithUser } from '@interfaces/auth.interface';
import { User } from '@interfaces/users.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { validationMiddleware } from '@middlewares/validation.middleware';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import { PaginationOptionsDto } from '@/dtos/pagination-options.dto';
import ProjectService from '@/services/project.service';
import { ProjectDto } from '@/dtos/project.dto';
import { Project } from '@/interfaces/project.interface';


@Controller()
export class ProjectController {

  public projectService = new ProjectService();

  @Post('/projects/add-project')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(ProjectDto, 'body'))
  @HttpCode(201)
  async addProject(@Req() req: RequestWithUser, @Body() projectRequest: ProjectDto) {
    const userData: User = req.user;
    const response: Project = await this.projectService.add(projectRequest, userData.id);
    return { data: response, message: 'Project added' };
  }

  @Put('/projects/:id')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(ProjectDto, 'body', true))
  @HttpCode(201)
  async editProject(@Req() req: RequestWithUser, @Param('id') projectId: number, @Body() projectData: ProjectDto) {
    const userData: User = req.user;
    const updatedProjectData: Project = await this.projectService.edit(projectData, projectId, userData.id);
    return { data: updatedProjectData, message: 'Edited Project' };
  }

  @Get('/projects/:id')
  @UseBefore(authMiddleware)
  async getProjectById(@Param('id') projectId: number) {
    const findOneProjectData: Project = await this.projectService.findProjectById(projectId);
    return { data: findOneProjectData, message: 'findOne' };
  }

  @Post('/projects/all')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(PaginationOptionsDto, 'body'))
  async getProjectsByPagination(@Req() req: RequestWithUser, @Body() paginationOptions: PaginationOptions) {
    const userData: User = req.user;
    const findAllProject: Pagination<Project> = await this.projectService.getAllProjectsByPagination(paginationOptions, userData.id);
    return { data: findAllProject, message: 'findAll' };
  }

  @Delete('/projects/:id')
  @UseBefore(authMiddleware)
  @HttpCode(200)
  async deleteProject(@Req() req: RequestWithUser, @Param('id') projectId: number) {
    const userData: User = req.user;
    const updatedProjectData: Project = await this.projectService.deleteProject(projectId, userData.id);
    return { data: updatedProjectData, message: 'Edited Project' };
  }


}
