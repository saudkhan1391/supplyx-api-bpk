import { Controller, Req, Body, Post, Get, Param, Put, UseBefore, HttpCode, Res } from 'routing-controllers';
import { CreateCompanyDto } from '@dtos/company.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Company } from '@interfaces/company.interface';
import { User } from '@interfaces/users.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { validationMiddleware } from '@middlewares/validation.middleware';
import CompanyService from '@services/company.service';

@Controller()
export class CompanyController {
  public companyService = new CompanyService();

  @Post('/company')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateCompanyDto, 'body'))
  @HttpCode(201)
  async addCompany(@Req() req: RequestWithUser, @Body() companyData: CreateCompanyDto) {
    const userData: User = req.user;
    const addCompanyProfile: Company = await this.companyService.add(companyData, userData.id);
    return { data: addCompanyProfile, message: 'Add company Profile' };
  }

  @Put('/company')
  @UseBefore(authMiddleware)
  @HttpCode(200)
  async updateProductStatus(@Req() req: RequestWithUser, @Body() companyData: CreateCompanyDto ) {
    const userData: User = req.user
    const company: Company = await this.companyService.edit(companyData, userData.id);
    return { data: company, message: 'Company Updated' };
  }

  @Get('/company')
  @UseBefore(authMiddleware)
  async getCompanyById(@Req() req: RequestWithUser) {
    const userData: User = req.user
    const company: Company = await this.companyService.findCompanyById(userData.id);
    return { data: company, message: 'findOne' };
  }

}
