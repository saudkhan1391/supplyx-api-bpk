import { Controller, Req, Body, Post, Get, Param, UseBefore, HttpCode, Delete } from 'routing-controllers';
import { RequestWithUser } from '@interfaces/auth.interface';
import { User } from '@interfaces/users.interface';
import { validationMiddleware } from '@middlewares/validation.middleware';
import { CatalogProductMappingRequestDto } from '@/dtos/catalog-product-mapping-request.dto';
import CatalogProductMappingService from '@/services/catalog-product.service';
import { CatalogProductMappingEntity } from '@/entity/catalog-product-mapping.entity';
import { ProductEntity } from '@/entity/product.entity';
import authMiddleware from '@/middlewares/auth.middleware';
import { Pagination } from '@/models/pagination.model';

@Controller()
export default class CatalogProductMappingController {

  public catalogProductMappingService = new CatalogProductMappingService();

  @Post('/catalogs')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CatalogProductMappingRequestDto, 'body'))
  @HttpCode(201)
  async addProductsIntoCatalog(@Req() req: RequestWithUser, @Body() catalogProductData: CatalogProductMappingRequestDto) {
    const userData: User = req.user;
    const products: CatalogProductMappingEntity[] = await this.catalogProductMappingService.add(catalogProductData); //this 1 needs to be mapped
    return { data: products, message: 'Add Products into Catalog' };
  }

  @Post('/catalogs/:id')
  @UseBefore(authMiddleware)
  async getProductsByCatalogId(@Req() req: RequestWithUser, @Param('id') catalogId: number) {
    const userData: User = req.user;
    const products: Pagination<ProductEntity> = await this.catalogProductMappingService.findProductsByCatalogId(catalogId, userData.id);
    return { data: products, message: 'find products by catalog id' };
  }

  @Delete('/catalogs')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CatalogProductMappingRequestDto, 'body'))
  async deleteProductsFromCatalog(@Req() req: RequestWithUser, @Body() catalogProductData: CatalogProductMappingRequestDto ) {
    const userData: User = req.user;
    await this.catalogProductMappingService.deleteProductFromCatalog(catalogProductData);
    return { message: 'Delete product mapping from catalog' };
  } 

}
