import { Controller, Req, Body, Post, Get, Param, Put, UseBefore, HttpCode } from 'routing-controllers';
import { RequestWithUser } from '@interfaces/auth.interface';
import { User } from '@interfaces/users.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { validationMiddleware } from '@middlewares/validation.middleware';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import { PaginationOptionsDto } from '@/dtos/pagination-options.dto';
import ProjectService from '@/services/project.service';
import { Project } from '@/interfaces/project.interface';
import BuildingService from '@/services/building.service';
import { BuildingDto } from '@/dtos/building.dto';
import { Building } from '@/interfaces/building.interface';


@Controller()
export class BuildingController {

  public buildingService = new BuildingService();

  @Post('/buildings/add-building')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(BuildingDto, 'body'))
  @HttpCode(201)
  async addProject(@Req() req: RequestWithUser, @Body() buildingRequest: BuildingDto) {
    const userData: User = req.user;
    const response: Building = await this.buildingService.add(buildingRequest, userData.id);
    return { data: response, message: 'Building added' };
  }

}
