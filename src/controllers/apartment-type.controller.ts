import { Controller, Body, Post, UseBefore, HttpCode, Delete, Param, Req } from 'routing-controllers';
import authMiddleware from '@middlewares/auth.middleware';
import { validationMiddleware } from '@middlewares/validation.middleware';
import ApartmentTypeService from '@/services/apartment-type.service';
import { ApartmentTypeDto } from '@/dtos/apartment-type.dto';
import { ApartmentType } from '@/interfaces/apartment-type.interface';
import { RequestWithUser } from '@/interfaces/auth.interface';


@Controller()
export class ApartmentTypeController {

  public apartmentTypeService = new ApartmentTypeService();

  @Post('/apartments/add')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(ApartmentTypeDto, 'body'))
  @HttpCode(201)
  async addProject(@Body() apartmentTypeRequest: ApartmentTypeDto) {
    const response: ApartmentType = await this.apartmentTypeService.add(apartmentTypeRequest);
    return { data: response, message: 'Apartment Type added' };
  }

  @Delete('/apartments/:id')
  @UseBefore(authMiddleware)
  @HttpCode(200)
    async deleteFile(@Req() req: RequestWithUser, @Param('id') id: number) {
        const deleteResponse: ApartmentType = await this.apartmentTypeService.delete(id);
        return { data: deleteResponse, message: 'delete object' };
    }

}
