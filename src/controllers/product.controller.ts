import { Controller, Req, Body, Post, Get, Param, Put, UseBefore, HttpCode } from 'routing-controllers';
import { CreateProductDto } from '@dtos/product.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Product } from '@interfaces/product.interface';
import { User } from '@interfaces/users.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { validationMiddleware } from '@middlewares/validation.middleware';
import ProductService from '@services/product.service';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import { PaginationOptionsDto } from '@/dtos/pagination-options.dto';
import { CreateDraftProductDto } from '@/dtos/draft-product.dto';


@Controller()
export class ProductController {

  public productService = new ProductService();

  @Get('/products/all')
  @UseBefore(authMiddleware)
  async getAllProductsData() {
    const products: Product[] = await this.productService.getAllProductsData();
    return { data: products, message: 'get all products data' };
  }

  @Post('/products/add-product')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateProductDto, 'body'))
  @HttpCode(201)
  async addProduct(@Req() req: RequestWithUser, @Body() productData: CreateProductDto) {
    const userData: User = req.user;
    const addProduct: Product = await this.productService.add(productData, userData.id);
    return { data: addProduct, message: 'Add Product' };
  }

  @Put('/products/:id')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateProductDto, 'body', true))
  @HttpCode(201)
  async editProduct(@Req() req: RequestWithUser, @Param('id') productId: number, @Body() productData: CreateProductDto) {
    const userData: User = req.user;
    const editProduct: Product = await this.productService.edit(productData, productId, userData.id);
    return { data: editProduct, message: 'Edit Product' };
  }

  @Get('/products/:id')
  @UseBefore(authMiddleware)
  async getProductById(@Param('id') productId: number) {
    const findOneProductData: Product = await this.productService.findProductById(productId);
    return { data: findOneProductData, message: 'findOne' };
  }

   @Get('/products/all/:key')
  @UseBefore(authMiddleware)
  async getProductByKey(@Param('key')  productId:string ) {
     const findOneProductData: Product[]= await this.productService.findProductsByKey(productId);
     
    return { data: findOneProductData, message: 'get products by key ' };
  }
    

  @Post('/products/all')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(PaginationOptionsDto, 'body'))
  async fetchProducts(@Req() req: RequestWithUser, @Body() paginationOptions: PaginationOptions) {
    const userData: User = req.user;
    const findAllProduct: Pagination<Product> = await this.productService.getAllProducts(paginationOptions, userData.id);
    return { data: findAllProduct, message: 'findOne' };
  }

  @Post('/products/add-draft-product')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateDraftProductDto, 'body'))
  @HttpCode(201)
  async addDraftProduct(@Req() req: RequestWithUser, @Body() productData: CreateDraftProductDto) {
    const userData: User = req.user;
    const addProduct: Product = await this.productService.addDraftProduct(productData, userData.id);
    return { data: addProduct, message: 'Add Draft Product' };
  }

  @Post('/products/status/:id')
  @UseBefore(authMiddleware)
  @HttpCode(200)
  async updateProductStatus(@Req() req: RequestWithUser, @Param('id') productId: number, @Body() status: { status: string } ) {
    const userData: User = req.user
    const product: Product = await this.productService.updateProductByStatus(productId, status.status, userData.id);
    return { data: product, message: 'update Product Status' };
  }

}
