import { Controller, Req, Body, Post, Get, UseBefore, HttpCode } from 'routing-controllers';
import { RequestWithUser } from '@interfaces/auth.interface';
import { User } from '@interfaces/users.interface';
import authMiddleware from '@middlewares/auth.middleware';
import CategoryService from '@/services/category.service';
import Category from '@/interfaces/category.interface';
import { CategoryDto } from '@/dtos/category.dto';


@Controller()
export class CategoryController {

  public categoryService = new CategoryService();

  @Post('/categories')
  @UseBefore(authMiddleware)
  @HttpCode(201)
  async addCategory(@Req() req: RequestWithUser, @Body() category: CategoryDto) {
    const userData: User = req.user;
    const addedCategory: Category = await this.categoryService.add(category, userData.id);
    return {  data: addedCategory,  message: 'Add Category' };
  }

  @Get('/categories/all')
  @UseBefore(authMiddleware)
  async getCategories() {
    const categories: Category[] = await this.categoryService.getAllCategories();
    return { data: categories, message: 'get all categories' };
  }

}
