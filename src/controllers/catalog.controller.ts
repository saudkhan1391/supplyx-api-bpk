import { Controller, Req, Body, Post, Get, Param, Put, UseBefore, HttpCode, Res } from 'routing-controllers';
import { CreateCatalogDto } from '@dtos/catalog.dto';
import { RequestWithUser } from '@interfaces/auth.interface';
import { Catalog } from '@interfaces/catalog.interface';
import { User } from '@interfaces/users.interface';
import authMiddleware from '@middlewares/auth.middleware';
import { validationMiddleware } from '@middlewares/validation.middleware';
import CatalogService from '@services/catalog.service';
import { Pagination, PaginationOptions } from '@/models/pagination.model';

@Controller()
export class CatalogController {

  public catalogService = new CatalogService();

  @Post('/catalog')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateCatalogDto, 'body'))
  @HttpCode(201)
  async addCatalog(@Req() req: RequestWithUser, @Body() catalogData: CreateCatalogDto) {
    const userData: User = req.user;
    const addCatalog: Catalog = await this.catalogService.add(catalogData, userData.id);
    return { data: addCatalog, message: 'Add Catalog' };
  }

  @Put('/catalog')
  @UseBefore(authMiddleware)
  @UseBefore(validationMiddleware(CreateCatalogDto, 'body', true))
  @HttpCode(200)
  async editCatalog(@Req() req: RequestWithUser, @Body() catalogData: CreateCatalogDto) {
    const userData: User = req.user;
    const editCatalog: Catalog = await this.catalogService.edit(catalogData, userData.id);
    return { data: editCatalog, message: 'Edit Catalog' };
  }

  @Get('/catalog/:id')
  @UseBefore(authMiddleware)
  async getCatalogById(@Param('id') catalogId: number) {
    const findOneCatalogData: Catalog = await this.catalogService.findCatalogById(catalogId);
    return { data: findOneCatalogData, message: 'find catalog' };
  }

  @Post('/catalog/all')
  @UseBefore(authMiddleware)
  async getCatalogs(@Req() req: RequestWithUser, @Body() paginationOptions: PaginationOptions) {
    const userData: User = req.user;
    const findAllCatalogs: Pagination<Catalog> = await this.catalogService.findAllCatalogs(paginationOptions, userData.id);
    return { data: findAllCatalogs, message: 'findAll catalog' };
  }
}
