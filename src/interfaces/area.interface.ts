import { ApartmentType } from '@interfaces/apartment-type.interface';

export interface Area {
  id: number;
  apartmentType: ApartmentType;
  name: string;
  count: number;
}
