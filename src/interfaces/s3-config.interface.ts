export interface S3Config {
    accessKeyId: string,
    secretAccessKey: string,
    supplyxBucketName: string,
    bucketUrl: string
}