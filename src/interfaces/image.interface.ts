
import { Company } from '@interfaces/company.interface';
import { Project } from 'aws-sdk/clients/codebuild';
import { Product } from "./product.interface";
import { User } from './users.interface';

export interface Attachment {
    id: number;
    product: Product;
    key: string;
    url: string;
}

export interface CompanyAttachment {
    id: number;
    company: Company;
    key: string;
    url: string;
}

export interface ProjectAttachment {
    id: number;
    key: string;
    url: string;
    project: Project;
}

export interface ProjectImage {
    id: number;
    key: string;
    url: string;
}

export interface UserImage {
    id: number;
    user: User;
    key: string;
    url: string;
}


/* export interface CompanyImage extends CompanyAttachment {
    id: number;
    company: Company;
    key: string;
    url: string;
}

export interface ProfileImage extends Attachment{
    id: number;
    company: Company;
    key: string;
    url: string;
}

export interface AboutImage extends Attachment {
    id: number;
    company: Company;
    key: string;
    url: string;
}

export interface ChooseImage extends Attachment {
    id: number;
    company: Company;
    key: string;
    url: string;
} */