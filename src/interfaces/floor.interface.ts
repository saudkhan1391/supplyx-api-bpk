import { Building } from '@interfaces/building.interface';
import { Apartment } from './apartment.interface';

export interface Floor {
  id: number;
  name: string;
  floorNumber: number;
  building: Building;
  apartments: Apartment[]
}
