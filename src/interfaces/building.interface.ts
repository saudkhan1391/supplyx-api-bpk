import { Apartment } from './apartment.interface';
import { Floor } from './floor.interface';
import { Project } from './project.interface';

export interface Building {
  id: number;
  project: Project;
  name: string;
  floorCount: string;
  floors: Floor[];
}


