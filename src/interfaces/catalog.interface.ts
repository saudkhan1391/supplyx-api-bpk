import { Product } from './product.interface';

export interface Catalog {
  id: number;
  userId: number;
  catalogName: string;
  description: string;
  // productCount: number;
  products: Product[];
}
