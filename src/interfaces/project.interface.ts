import { Address } from './address.interface';
import { ApartmentType } from './apartment-type.interface';
import { Building } from './building.interface';
import { ProjectAttachment, ProjectImage } from './image.interface';

export interface Project {
  id: number;
  userId: number;
  name: string;
  type: string;
  description: string;
  status: string;
  address: Address;
  image: ProjectImage;
  buildings: Building[];
  apartmentTypes: ApartmentType[];
  specifications: ProjectAttachment[]
}
