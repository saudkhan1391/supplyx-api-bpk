import { Floor } from '@interfaces/floor.interface';
import { ApartmentType } from './apartment-type.interface';

export interface Apartment {
  id: number;
  floor: Floor;
  apartmentType: ApartmentType[];
}
