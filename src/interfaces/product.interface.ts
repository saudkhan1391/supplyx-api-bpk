import { Catalog } from './catalog.interface';
import Category from './category.interface';
import { Attachment } from './image.interface';

export interface Product {
  id: number;
  userId: number;
  productName: string;
  category: Category;
  brand: string;
  manufacturer: string;
  partNo: string;
  minimumQuantity: number;
  availableQuantity: number;
  shipinfo: string;
  hsCode: string;
  description: string;
  price: string;
  images: Attachment[];
  status: string
  catalog: Catalog
}
