import { CompanyAttachment } from './image.interface';


export interface Company {
    id: number;
    userId: number;
    name: string;
    banner: string;
    aboutImage: CompanyAttachment[];
    whyChooseUs: string;
    images: CompanyAttachment[];
    bannerImage: CompanyAttachment[];
    chooseusImage: CompanyAttachment[];
    email: string;
    phone: string;
    mobile: string;
    youtubeChannel: string;
    street: string;
    postalCode: string;
    website: string;
  }
  