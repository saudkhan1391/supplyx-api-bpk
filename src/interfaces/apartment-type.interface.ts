import { Area } from '@interfaces/area.interface';
import { Project } from './project.interface';

export interface ApartmentType {
  id: number;
  project: Project;
  name: string;
  size: number;
  areas: Area[];
}
