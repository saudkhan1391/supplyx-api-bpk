import { UserImage } from "./image.interface";

export interface User {
  id: number;
  email: string;
  password: string;
  firstName:string;
  lastName:string;
  companyName:string;
  roleType:string;
  country:string;
  zipCode:number;
  mobile:string;
  phone:string;
  phoneCode: string;
  street:string;
  state:string;
  images: UserImage[];
}


