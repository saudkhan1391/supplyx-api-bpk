process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

import 'dotenv/config';
import 'reflect-metadata';
import App from '@/app';
import { AuthController } from '@controllers/auth.controller';
import { IndexController } from '@controllers/index.controller';
import { UsersController } from '@controllers/users.controller';
import { ProductController } from '@controllers/product.controller';
import { CatalogController } from '@controllers/catalog.controller';
import { CompanyController } from '@controllers/company.controller';
import validateEnv from '@utils/validateEnv';
import CatalogProductMappingController from './controllers/catalog-product-mapping.controller';
import { FileUploadController } from './controllers/file-upload.controller';
import { CategoryController } from './controllers/category.controller';
import { ProjectController } from '@controllers/project.controller';
import { BuildingController } from './controllers/building.controller';
import { ApartmentTypeController } from './controllers/apartment-type.controller';

validateEnv();

const app = new App([
  AuthController,
  IndexController,
  UsersController,
  ProductController,
  CatalogController,
  CompanyController,
  CatalogProductMappingController,
  CategoryController,
  FileUploadController,
  ProjectController,
  BuildingController,
  ApartmentTypeController
]);
app.listen();
