import * as bcrypt from 'bcryptjs';
import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { User } from '@interfaces/users.interface';
import { isEmpty } from '@utils/util';
import { DeleteResult, getRepository, Repository } from 'typeorm';
import { UserEntity } from '@/entity/users.entity';
import { Pagination, PaginationOptions } from '@/models/pagination.model'

class UserService {

  public async findAllUser(paginationOptions: PaginationOptions): Promise<Pagination<User>>{
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const [results, total] = await userRepo.findAndCount({
        take: paginationOptions.take,
        skip: paginationOptions.page, // think this needs to be page * limit
      });
    return {
        data: results,
        total: total
    };
  }

  public async findUserById(userId: number): Promise<User> {
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: User = await userRepo.findOne(userId);
    if (!findUser) throw new HttpException(409, "User not found");
    return findUser;
  }

  public async createUser(userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);

    const findUser: User = await userRepo.findOne({"email":userData.email});
    console.log(findUser)
    if (findUser) throw new HttpException(409, `Your email ${userData.email} already exists`);

    const hashedPassword = await bcrypt.hash(userData.password, 10);
    const createUserData: User = {...userData, password: hashedPassword };
    const savedUser = await userRepo.save(createUserData);
    return savedUser;
  }

  public async updateUser(userId: number, userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: User = await userRepo.findOne({id: userId});
    console.log(findUser.email)
    if (!findUser) throw new HttpException(409, "You're not user");
   /*  if(findUser.images){
      findUser.images.splice(0);
      await userRepo.save(findUser);
    } */
    /* const hashedPassword = await bcrypt.hash(userData.password, 10); */
    const updateUserData: User = {id: userId, ...userData };
    const updatedUser = await userRepo.save(updateUserData);
    // const updateUserData: User = userRepo.update({ id: userId},{...userData, password: hashedPassword });

    return updatedUser;
  }

  public async deleteUser(userId: number): Promise<User> {
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: UserEntity = await userRepo.findOne(userId);
    if (!findUser) throw new HttpException(409, "You're not user");

    const deleteUserData: DeleteResult = await userRepo.delete(userId)
    return findUser;
  }
}

export default UserService;
