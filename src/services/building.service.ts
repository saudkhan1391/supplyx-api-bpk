import { HttpException } from '@exceptions/HttpException';
import { FindConditions, getRepository, Repository } from 'typeorm';
import { isEmpty } from '@utils/util';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import { ProjectDto } from '@/dtos/project.dto';
import { Project } from '@/interfaces/project.interface';
import { ProjectEntity } from '@/entity/project.entity';
import { BuildingDto } from '@/dtos/building.dto';
import { Building } from '@/interfaces/building.interface';
import { BuildingEntity } from '@/entity/building.entity';

class BuildingService {
  public async add(request: BuildingDto, useId: number): Promise<Building> {
    if (isEmpty(request)) throw new HttpException(400, "You're not userData");
    const buildingRepo: Repository<BuildingEntity> = getRepository(BuildingEntity);
   // const projectRequest: Building = { ...request, userId: useId };
    const savedBuilding = await buildingRepo.save(request);
    return savedBuilding;
  }

}

export default BuildingService;
