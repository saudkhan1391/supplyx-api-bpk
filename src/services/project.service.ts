import { HttpException } from '@exceptions/HttpException';
import { FindConditions, getRepository, Repository } from 'typeorm';
import { isEmpty } from '@utils/util';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import { ProjectDto } from '@/dtos/project.dto';
import { Project } from '@/interfaces/project.interface';
import { ProjectEntity } from '@/entity/project.entity';

class ProjectService {
  
  public async add(request: ProjectDto, useId: number): Promise<Project> {
    if (isEmpty(request)) throw new HttpException(400, "You're not userData");
    const projectRepo: Repository<ProjectEntity> = getRepository(ProjectEntity);
    const projectRequest: Project = { ...request, userId: useId };
    const savedProject = await projectRepo.save(projectRequest);
    return savedProject;
  }

   public async edit(project: ProjectDto, projectId:number, useId: number): Promise<Project> {
    if (isEmpty(project)) throw new HttpException(400, "No request");
    const projectRepo: Repository<ProjectEntity> = getRepository(ProjectEntity);
    const retreivedProject: Project = await projectRepo.findOne({
      id: projectId
    });
    if (!retreivedProject) throw new HttpException(409, `Catalog does not exist`);
    const updateProjectData: Project = { ...project, userId: useId };
    const updatedProjectData = await projectRepo.save(updateProjectData);
    return updatedProjectData;
  }

  public async findProjectById(projectId: number): Promise<Project> {
    const projectRepo: Repository<ProjectEntity> = getRepository(ProjectEntity);
    const findProject: Project = await projectRepo.findOne({ id: projectId});
    if (!findProject) throw new HttpException(404, 'Project not found');
    return findProject;
  }

  public async getAllProjectsByPagination(paginationOptions: PaginationOptions, userId: number):  Promise<Pagination<Project>> {
    const projectRepo: Repository<ProjectEntity> = getRepository(ProjectEntity);
    let whereClause: FindConditions<ProjectEntity> = {
      userId: userId
    }

    /* const [results, total] = await projectRepo.findAndCount({
      where: whereClause,
      order: { id: "DESC" },
      take: paginationOptions.take,
      skip: paginationOptions.page * paginationOptions.take, // think this needs to be page * limit
    }); */
    const data = await projectRepo.find({
      userId: userId
    })
    return {
      data: data,
      total: 10,
    };
  }

  public async deleteProject(projectId: number, userId: number) {
    const projectRepo: Repository<ProjectEntity> = getRepository(ProjectEntity);
    const project: Project = await projectRepo.findOne({
      id: projectId,
      userId: userId
    });
    if(!project) throw new HttpException(409, `Project not found for ${projectId}`);
    await projectRepo.delete(projectId);
    return project;
  }

}

export default ProjectService;
