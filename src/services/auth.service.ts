import * as bcrypt from 'bcryptjs';
import config from 'config';
import jwt from 'jsonwebtoken';
import { CreateUserDto } from '@dtos/users.dto';
import { LoginUserDto } from '@/dtos/LoginUserDto';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken, TokenData } from '@interfaces/auth.interface';
import { User } from '@interfaces/users.interface';
import userModel from '@models/users.model';
import { getRepository, Repository } from 'typeorm';
import { UserEntity } from '@/entity/users.entity';
import { isEmpty } from '@utils/util';
import CompanyService from './company.service';
import { CreateCompanyDto } from '@/dtos/company.dto';

class AuthService {
  public users = userModel;
  public companyService = new CompanyService();

  public async signup(userData: CreateUserDto): Promise<User> {
    if (isEmpty(userData)) {
      throw new HttpException(400, "You're not userData");
    }
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: User = await userRepo.findOne({"email": userData.email});
    if (findUser) throw new HttpException(409, `You're email ${userData.email} already exists`);
    if(userData.password !== userData.confirmPassword) {
      throw new HttpException(409, `confirm password do not match with passowrd`);
    }
    delete userData.confirmPassword
    const hashedPassword = await bcrypt.hash(userData.password, 10);
    const createUserData: User = {...userData, password: hashedPassword };
    const savedUser = await userRepo.save(createUserData);
    if(savedUser) {
      await this.companyService.add({
        name: userData.companyName
      } as CreateCompanyDto, savedUser.id)
    }
    return savedUser;
  }

  public async login(userData: LoginUserDto): Promise<{ findUser: User }> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");

    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: User = await userRepo.findOne({"email":userData.email/* ,roleType:userData.roleType */});
    if (!findUser) throw new HttpException(409, `Your email ${userData.email} not found`);

    const isPasswordMatching: boolean = await bcrypt.compare(userData.password, findUser.password);
    if (!isPasswordMatching) throw new HttpException(409, "You're password not matching");

    const tokenData = this.createToken(findUser);
    // const cookie = this.createCookie(tokenData);
    delete findUser.password
    findUser['token']= tokenData.token

    return {findUser };
  }

  public async logout(userData: User): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, "You're not userData");

    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: User = await userRepo.findOne({"email":userData.email,roleType:userData.roleType});
    if (!findUser) throw new HttpException(409, "You're not user");

    return findUser;
  }

  public createToken(user: User): TokenData {
    const dataStoredInToken: DataStoredInToken = { id: user.id,email:user.email,roleType:user.roleType };
    const secretKey: string = config.get('secretKey');
    const expiresIn: number = 60 * 60 * 12;

    return { expiresIn, token: jwt.sign(dataStoredInToken, secretKey, { expiresIn }) };
  }

  public async edit(userData: CreateUserDto, userId: number): Promise<User> {
    if(isEmpty(userData)){
      throw new HttpException(400, "You are not User");
    }
    const userRepo: Repository<UserEntity> = getRepository(UserEntity);
    const findUser: User = await userRepo.findOne({id: userId});
    if (!findUser) throw new HttpException(409, `User does not exist`);
    const updatedUser: User ={
      ...userData,
      id: userId
    }
    const savedUser = await userRepo.save(updatedUser);
    return savedUser
  }
  
}

export default AuthService;
