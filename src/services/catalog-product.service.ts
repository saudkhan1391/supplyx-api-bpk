import { CatalogProductMappingRequestDto } from "@/dtos/catalog-product-mapping-request.dto";
import { CatalogProductMappingEntity } from "@/entity/catalog-product-mapping.entity";
import { ProductEntity } from "@/entity/product.entity";
import { HttpException } from "@/exceptions/HttpException";
import { Pagination } from "@/models/pagination.model";
import { isEmpty } from "class-validator";
import { getRepository, Repository } from "typeorm";
import CatalogService from "./catalog.service";
import ProductService from "./product.service";


class CatalogProductMappingService {

  public catalogService = new CatalogService();

  public async add(catalogProductData: CatalogProductMappingRequestDto): Promise<CatalogProductMappingEntity[]> {
    if (isEmpty(catalogProductData)) throw new HttpException(400, "You're not userData");
    const catalogProductRepo: Repository<CatalogProductMappingEntity> = getRepository(CatalogProductMappingEntity);
    const catalogProductList: CatalogProductMappingEntity[] = catalogProductData.productIds.map( productId => ({
      catalogId: catalogProductData.id,
      productId: productId
    } as CatalogProductMappingEntity))
    const savedProducts = await catalogProductRepo.save(catalogProductList);
    await this.catalogService.updateProductCount(catalogProductData.id,catalogProductData.productIds.length, true );
    return savedProducts;
  }

  public async findProductsByCatalogId(catalogId: number, userId: number): Promise<Pagination<ProductEntity>> {
    const catalogProductRepo: Repository<CatalogProductMappingEntity> = getRepository(CatalogProductMappingEntity);
    const catalogProuctList: CatalogProductMappingEntity[] = await catalogProductRepo.find({ 
      catalogId: catalogId
    });
    if (!catalogProuctList){
      throw new HttpException(409, 'Catalog Product Mapping not found');
    } 
    const productIds : number[] = catalogProuctList.map(catalog => catalog.productId);
    const productService : ProductService = new ProductService();
    return await productService.getProductsByIds(productIds, userId);
  }

  public async deleteProductFromCatalog(catalogData: CatalogProductMappingRequestDto) {
    if (isEmpty(catalogData)) throw new HttpException(400, "You're not userData");
    await getRepository(CatalogProductMappingEntity)
    .createQueryBuilder()
    .delete()
    .from(CatalogProductMappingEntity)
    .where("catalogId = :catalogId", { catalogId: catalogData.id })
    .andWhere("productId IN (:...productIdList)", {productIdList : catalogData.productIds})
    .execute();
    await this.catalogService.updateProductCount(catalogData.id,catalogData.productIds.length, false );
  } 

}

export default CatalogProductMappingService;
