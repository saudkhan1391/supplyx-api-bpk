import { HttpException } from '@exceptions/HttpException';
import { getRepository, Repository } from 'typeorm';
import { isEmpty } from '@utils/util';
import { ApartmentTypeDto } from '@/dtos/apartment-type.dto';
import { ApartmentType } from '@/interfaces/apartment-type.interface';
import { ApartmentTypeEntity } from '@/entity/apartment-type.entity';

class ApartmentTypeService {
  
  public async add(apartmentType: ApartmentTypeDto): Promise<ApartmentType> {
    if (isEmpty(apartmentType)) throw new HttpException(400, "Bad Request for adding apartment type");
    const apartmentTypeRepo: Repository<ApartmentTypeEntity> = getRepository(ApartmentTypeEntity);
    const savedApartmentType = await apartmentTypeRepo.save(apartmentType);
    return savedApartmentType;
  }

  public async delete(id: number ): Promise<ApartmentType> {
    const apartmentTypeRepo: Repository<ApartmentTypeEntity> = getRepository(ApartmentTypeEntity);
    const apartmentType: ApartmentType = await apartmentTypeRepo.findOne({
      id: id
    });
    if (!apartmentType) throw new HttpException(409, `Apartment Type not found for ${id}`);
    await apartmentTypeRepo.delete(id);
    return apartmentType;
  }

}

export default ApartmentTypeService;
