import { CreateCatalogDto } from '@dtos/catalog.dto';
import { HttpException } from '@exceptions/HttpException';
import { Catalog } from '@interfaces/catalog.interface';
import { getRepository, Repository } from 'typeorm';
import { CatalogEntity } from '@/entity/catalog.entity';
import { isEmpty } from '@utils/util';
import { Pagination, PaginationOptions} from '@/models/pagination.model';
import { logger } from '@utils/logger';

class AddressService {

  public async add(catalogData: CreateCatalogDto, useId: number): Promise<Catalog> {
    if (isEmpty(catalogData)) throw new HttpException(400, "You're not userData");
    const catalogRepo: Repository<CatalogEntity> = getRepository(CatalogEntity);
    const createCatalogData: Catalog = { ...catalogData, userId: useId };
    const savedCatalog = await catalogRepo.save(createCatalogData);
    return savedCatalog;
  }

  public async edit(catalogData: CreateCatalogDto, useId: number): Promise<Catalog> {
    if (isEmpty(catalogData)) throw new HttpException(400, "You're not productData");
    const catalogRepo: Repository<CatalogEntity> = getRepository(CatalogEntity);
    const findCatalog: Catalog = await catalogRepo.findOne({
      id: catalogData.id
    });
    if (!findCatalog) throw new HttpException(409, `Catalog does not exist`);
    const updatecCatalogData: Catalog = { ...catalogData, userId: useId };
    const savedCatalog = await catalogRepo.save(updatecCatalogData);
    return savedCatalog;
  }

  public async findCatalogById(catalogId: number): Promise<Catalog> {
    const catalogRepo: Repository<CatalogEntity> = getRepository(CatalogEntity);
    const findCatalog: Catalog = await catalogRepo.findOne({ id: catalogId});
    if (!findCatalog) throw new HttpException(409, 'Catalog not found');
    return findCatalog;
  }

  public async findAllCatalogs(paginationOptions: PaginationOptions, userId: number): Promise<Pagination<Catalog>> {
    logger.info(`Catalog fetched for userid: ${userId} for request with page: ${paginationOptions.page} and skip: ${paginationOptions.take}`)
    const catalogRepo: Repository<CatalogEntity> = getRepository(CatalogEntity);
    const [results, total] = await catalogRepo.findAndCount({
      where: { 
        userId: userId
      }, 
      order: { id: "ASC" },
      take: paginationOptions.take,
      skip: paginationOptions.page * paginationOptions.take, // think this needs to be page * limit
    });
    return {
      data: results,
      total: total,
    };
  }

  public async updateProductCount(catalogId: number, productCount: number, isProductAdded : boolean) {
    const catalogRepo: Repository<CatalogEntity> = getRepository(CatalogEntity);
    const catalog: CatalogEntity = await catalogRepo.findOne({id: catalogId});
    
    let productIdCount: number = 0;
    if(isProductAdded)
      productIdCount = catalog.productCount + productCount;
    else
      productIdCount = catalog.productCount - productCount;

      //update product count
    await catalogRepo.createQueryBuilder()
      .update(CatalogEntity)
      .set({productCount: productIdCount })
      .where("id = :id", { id: catalogId })
      .execute();
  }
}

export default AddressService;
