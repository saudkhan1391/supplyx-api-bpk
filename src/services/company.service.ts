import { CreateCompanyDto } from '@dtos/company.dto';
import { HttpException } from '@exceptions/HttpException';
import { Company } from '@interfaces/company.interface';
import { getRepository, Repository } from 'typeorm';
import { CompanyEntity } from '@/entity/company.entity';
import { isEmpty } from '@utils/util';

class CompanyService {

  public async add(companyData: CreateCompanyDto, userId: number): Promise<CompanyEntity> {
    if (isEmpty(companyData)) throw new HttpException(400, "You're not userData");
    const companyRepo: Repository<CompanyEntity> = getRepository(CompanyEntity);
    const createCompanyData: Company = { ...companyData, userId: userId };
    const savedCompany: CompanyEntity = await companyRepo.save(createCompanyData);
    return savedCompany;
  }

  public async edit(companyData: CreateCompanyDto, userId: number): Promise<Company> {
    if (isEmpty(companyData)){ 
      throw new HttpException(400, "You're not productData");
    }
    const companyRepo: Repository<CompanyEntity> = getRepository(CompanyEntity);
    const findCompany: Company = await companyRepo.findOne({ userId: userId });
    if (!findCompany) throw new HttpException(409, `Company does not exists`);
    const updatedCompany: Company = {
      ...companyData,
      userId: userId
    }
    const savedCompany = await companyRepo.save(updatedCompany);
    return savedCompany;
  }

  public async findCompanyById(userId: number): Promise<Company> {
    const companyRepo: Repository<CompanyEntity> = getRepository(CompanyEntity);
    const company: Company = await companyRepo.findOne({ userId: userId});
    return company;
  }

}

export default CompanyService;
