import AWS from 'aws-sdk';
import { S3Config } from '@/interfaces/s3-config.interface';
import { logger } from '@/utils/logger';
import config from 'config';
import * as fs from 'fs';
import internal from 'stream';
import { UploadedFile } from '@/models/file-uploaded.model';

const { accessKeyId, secretAccessKey, supplyxBucketName, bucketUrl }: S3Config = config.get('aws');

class AwsS3Service {

    s3: AWS.S3 = new AWS.S3({
        accessKeyId: accessKeyId,
        secretAccessKey: secretAccessKey
    });

    /**
     * method to get file from AWS S3 Bucket
     * @param key 
     * @returns 
     */
    getObject( key: string): internal.Readable {
        const s3Data = {
            Bucket: supplyxBucketName,
            Key: key
        }
        const fileStream = this.s3.getObject(s3Data).createReadStream();
        logger.info('🚀🚀 File Stream read from S3 🚀🚀');
        logger.info("FileStream => ", fileStream);
        return fileStream;
    }

    getBucket(): AWS.S3 {
        return this.s3;
    }

    public async uploadToAWS( fileName: string ) {
        let videoUrl: string = '';
        fs.readFile(fileName, (err, data ) => {
            if(err) throw err;
            const params = {
                Bucket: supplyxBucketName, // pass your bucket name
                Key: fileName, // file will be saved as testBucket/contacts.csv
                Body: fs.readFileSync(fileName)
            };

            this.s3.upload(params, (s3Err, data) => {
                if(s3Err) throw s3Err;
                logger.info(`File uploaded successfully at ${data.Location}`);
                videoUrl = data.Location;
            });
        })
    }

    private generateFileKey(file: Express.Multer.File, timestamp: number): string {
        let extension: string = file.originalname.substring(file.originalname.lastIndexOf("."), file.originalname.length);
        return `${timestamp}-${file.originalname.replace(/([^a-z0-9.]+)/gi, '-').replace(/\.[^/.]+$/, "")}${extension}`;
    }
    
    private async uploadFile(file: Express.Multer.File): Promise<UploadedFile> {
        const timestamp = Date.now();
        const fileKey = this.generateFileKey(file, timestamp);
        await this.s3.putObject({
                Bucket: supplyxBucketName,
                Key: fileKey,
                ContentType: file.mimetype,
                Body: file.buffer,
                //ACL: s3Config.defaultFilesACL,
            }).promise();

        return {
            key: fileKey,
            path: `${bucketUrl}/${fileKey}`
        };
    }
    
    async upload( files: Express.Multer.File | Express.Multer.File[] ): Promise<UploadedFile | UploadedFile[] | undefined> {
        try {
          if (Array.isArray(files)) {
            const uploadedFiles = await Promise.all(
              files.map(async (file) => this.uploadFile(file))
            );
            return uploadedFiles
          }
    
          const uploadedFile: UploadedFile = await this.uploadFile(files);
          return uploadedFile;
        } catch {
          return undefined;
        }
    }

    async delete( key: string ): Promise<string> {
        await this.s3.deleteObject({
            Bucket: supplyxBucketName,
            Key: key
        }).promise();
        return key;
    }

}

export default AwsS3Service;