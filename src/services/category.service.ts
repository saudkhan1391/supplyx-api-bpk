import { getRepository, Repository } from 'typeorm';
import { CategoryEntity } from '@/entity/category.entity';
import Category from '@/interfaces/category.interface';
import { CategoryDto } from '@/dtos/category.dto';
import { HttpException } from '@/exceptions/HttpException';
import { isEmpty } from 'class-validator';

class CategoryService {

  public async getAllCategories(): Promise<Category[]> {
    const categoryRepo: Repository<CategoryEntity> = getRepository(CategoryEntity);
    const categories: Category[] = await categoryRepo.find();
    return categories;
  }

  public async add(category: CategoryDto, userId: number): Promise<Category> {
    if (isEmpty(category)) throw new HttpException(400, "You're not userData");
    const categoryRepo: Repository<CategoryEntity> = getRepository(CategoryEntity);
    const categoryData: Category = { ...category, userId: userId} ;
    const addedCategory = await categoryRepo.save(categoryData);
    return addedCategory;
  }
}

export default CategoryService;
