import { CreateProductDto } from '@dtos/product.dto';
import { HttpException } from '@exceptions/HttpException';
import { Product } from '@interfaces/product.interface';
import { getRepository, Repository, FindConditions, In, ILike } from 'typeorm';
import { ProductEntity } from '@/entity/product.entity';
import { isEmpty } from '@utils/util';
import { Pagination, PaginationOptions } from '@/models/pagination.model';
import { CreateDraftProductDto } from '@/dtos/draft-product.dto';

class ProductService {

  public async getAllProductsData(): Promise<Product[]> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const product: Product[] = await productRepo.find();
    return product;
  }

  public async add(productData: CreateProductDto, userId: number): Promise<Product> {
    if (isEmpty(productData)) throw new HttpException(400, "You're not userData");
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const createProductData: Product = { ...productData, userId: userId };
    const savedProduct = await productRepo.save(createProductData);
    return savedProduct;
  }

  public async edit(productData: CreateProductDto, productId: number, userId: number): Promise<Product> {
    if (isEmpty(productData)) throw new HttpException(400, "You're not productData");
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const findProduct: Product = await productRepo.findOne({ id: productId });
    if (!findProduct) throw new HttpException(409, `Your product does not exist`);
    const updatecProductData: Product = { id: productId, ...productData, userId: userId };
    const savedProduct = await productRepo.save(updatecProductData);
    return savedProduct;
  }

  public async findProductById(productId: number): Promise<Product> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const findProduct: Product = await productRepo.findOne({ id: productId });
    if (!findProduct) throw new HttpException(409, 'Product not found');
    return findProduct;
  }

  public async findProductsByKey(productId: string): Promise<Product[]> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const findProduct: Product[] = await productRepo.find({
      where: [{ brand: productId }, { productName: productId }, { partNo: productId }],
    });


    // const reposproduct: Product = {...findProduct}
    if (!findProduct) throw new HttpException(409, 'Product not found');
    return findProduct;
  }



  public async getAllProductsForBuyer(paginationOptions: PaginationOptions, useId: number): Promise<Pagination<Product>> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    let whereClause: FindConditions<ProductEntity> = paginationOptions.status ? {
      // userId: useId,
      status: paginationOptions.status
    } : {
      // userId: useId
    }

    paginationOptions.filter ? whereClause.productName = ILike(`%${paginationOptions.filter}%`) : whereClause;

    const [results, total] = await productRepo.findAndCount({
      where: whereClause,
      order: { id: "DESC" },
      take: paginationOptions.take,
      skip: paginationOptions.page * paginationOptions.take, // think this needs to be page * limit
    });
    return {
      data: results,
      total: total,
    };
  }

  public async getAllProducts(paginationOptions: PaginationOptions, useId: number): Promise<Pagination<Product>> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    let whereClause: FindConditions<ProductEntity> = paginationOptions.status ? {
      userId: useId,
      status: paginationOptions.status
    } : {
      userId: useId
    }

    paginationOptions.filter ? whereClause.productName = ILike(`%${paginationOptions.filter}%`) : whereClause;

    const [results, total] = await productRepo.findAndCount({
      where: whereClause,
      order: { id: "DESC" },
      take: paginationOptions.take,
      skip: paginationOptions.page * paginationOptions.take, // think this needs to be page * limit
    });
    return {
      data: results,
      total: total,
    };
  }

  public async getProductsById(productIds: number[]): Promise<ProductEntity[]> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const products: ProductEntity[] = await productRepo.findByIds(productIds);
    if (!products) throw new HttpException(409, 'Product not found');
    return products;
  }

  public async updateProductStatus(id: number, status: string) {
    await getRepository(ProductEntity)
      .createQueryBuilder()
      .update(ProductEntity)
      .set({ status: status })
      .where("id = :id", { id: id })
      .execute();
  }

  public async getProductsByIds(productIds: number[], userId: number): Promise<Pagination<ProductEntity>> {
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const [results, total] = await productRepo.findAndCount({
      where: {
        id: In(productIds),
        userId: userId
      }
    });
    return {
      data: results,
      total: total
    };
  }

  public async addDraftProduct(productData: CreateDraftProductDto, userId: number): Promise<Product> {
    console.warn("productData", productData)
    if (isEmpty(productData)) throw new HttpException(400, "You're not userData");
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const createProductData: Product = { ...productData, userId: userId, status: 'draft' };
    console.log("createProductData", createProductData)
    const savedProduct = await productRepo.save(createProductData);
    console.log("savedProduct", savedProduct)
    return savedProduct;
  }

  public async updateProductByStatus(productId: number, status: string, userId: number): Promise<Product> {
    if (isEmpty(productId)) throw new HttpException(400, "You're not userData");
    const productRepo: Repository<ProductEntity> = getRepository(ProductEntity);
    const product: Product = await this.findProductById(productId);
    const createProductData: Product = { ...product, userId: userId, status: status };
    const savedProduct = await productRepo.save(createProductData);
    return savedProduct;
  }

}

export default ProductService;
