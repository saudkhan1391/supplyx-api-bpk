import { CompanyAttachment } from "@/interfaces/image.interface";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { CompanyEntity } from "./company.entity";

@Entity({ name: 'chooseimage' })
export class ChooseImageEntity extends BaseEntity implements CompanyAttachment {
 
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => CompanyEntity, company => company.chooseusImage)
  @JoinColumn({name: "companyId"})
  company: CompanyEntity;

  @Column('varchar')
  key: string;

  @Column('varchar')
  url: string
 
}