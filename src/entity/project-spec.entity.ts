import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ProjectAttachment } from '@interfaces/image.interface';
import { ProjectEntity } from './project.entity';
import { Project } from 'aws-sdk/clients/codebuild';

@Entity({ name: 'project-spec' })
export class ProjectSpecEntity extends BaseEntity implements ProjectAttachment {
 
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => ProjectEntity, { onDelete: "CASCADE", nullable: true })
  @JoinColumn({name: "productId"})
  project: Project;

  @Column('varchar')
  key: string;

  @Column('varchar')
  url: string
 
}