import { Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Catalog } from '@interfaces/catalog.interface';
import { BaseEntity } from './base.entity';
import { ProductEntity } from './product.entity';

@Entity({ name: 'catalog' })
export class CatalogEntity extends BaseEntity implements Catalog {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  userId: number;

  @Column()
  catalogName: string;

  @Column()
  description: string;

  @Column({ nullable: true })
  productCount: number;

  @OneToMany(() => ProductEntity, product => product.catalog)
  products: ProductEntity[];
}
