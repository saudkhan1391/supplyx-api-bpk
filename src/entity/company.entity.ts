import { Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { Company } from '@interfaces/company.interface';
import { BaseEntity } from './base.entity';
import { CompanyImageEntity } from './company-image.entity';
import { CompanyBannerEntity } from './banner-image.entity';
import { AboutImageEntity } from './about-image.entity';
import { ChooseImageEntity } from './choousImage.entity';

@Entity({ name: 'company' })
export class CompanyEntity extends BaseEntity implements Company {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('varchar', {nullable: true })
  userId: number;

  @Column('varchar', {nullable: true })
  banner: string;

  @Column('varchar', {nullable: true })
  name: string;

  @Column('varchar', {nullable: true })
  street: string;

  @Column('varchar', {nullable: true })
  postalCode: string;

  @Column('varchar', {nullable: true })
  about: string;

  @Column('varchar', {nullable: true })
  whyChooseUs: string;

  @Column('varchar', {nullable: true })
  whyChooseUsImage: string;

  @OneToMany(() => CompanyImageEntity, image => image.company, { cascade: true, onDelete: "CASCADE", eager: true, nullable: true})
  images: CompanyImageEntity[];

  @OneToMany(() => CompanyBannerEntity, image => image.company, { cascade: true, onDelete: "CASCADE", eager: true, nullable: true})
  bannerImage: CompanyBannerEntity[];

  @OneToMany(() => AboutImageEntity, image => image.company, { cascade: true, onDelete: "CASCADE", eager: true, nullable: true})
  aboutImage: AboutImageEntity[];

  @OneToMany(() => ChooseImageEntity, image => image.company, { cascade: true, onDelete: "CASCADE", eager: true, nullable: true})
  chooseusImage: ChooseImageEntity[];

  @Column('varchar', {nullable: true })
  email: string;

  @Column('varchar', {nullable: true })
  phone: string;

  @Column('varchar', {nullable: true })
  mobile: string;

  @Column('varchar', {nullable: true })
  website: string;

  @Column('varchar', {nullable: true })
  youtubeChannel: string;
 
}
