import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Attachment } from '@interfaces/image.interface';
import { ProductEntity } from './product.entity';

@Entity({ name: 'product-spec' })
export class ProductSpecEntity extends BaseEntity implements Attachment {
 
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => ProductEntity, product => product.specifications)
  @JoinColumn({name: "productId"})
  product: ProductEntity;

  @Column('varchar')
  key: string;

  @Column('varchar')
  url: string
 
}