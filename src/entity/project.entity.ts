import { Entity, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, JoinColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ProjectAddressEntity } from './address.entity';
import { Project } from '@/interfaces/project.interface';
import { BuildingEntity } from './building.entity';
import { ProjectImageEntity } from './project-image.entity';
import { ApartmentTypeEntity } from './apartment-type.entity';
import { ProjectAttachment } from '@/interfaces/image.interface';
import { ProjectSpecEntity } from './project-spec.entity';

@Entity({ name: 'project' })
export class ProjectEntity extends BaseEntity implements Project {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({nullable: true})
  userId: number;

  @Column()
  name: string;

  @Column({nullable: true})
  type: string;

  @Column({nullable: true})
  description: string;

  @Column({nullable: true})
  status: string

  @OneToOne(() => ProjectImageEntity, { cascade: true, eager: true, nullable: true })
  @JoinColumn({ name: 'imageId' })
  image: ProjectImageEntity;

  @OneToOne(() => ProjectAddressEntity, { cascade: true, eager: true, onDelete: "CASCADE", nullable: true })
  @JoinColumn({ name: 'addressId' })
  address: ProjectAddressEntity;

  @OneToMany(() => BuildingEntity, building => building.project, { cascade: true, eager: true, nullable: true})
  buildings: BuildingEntity[];

  @OneToMany(() => ApartmentTypeEntity, apartmentType => apartmentType.project, { cascade: true, eager: true, nullable: true})
  apartmentTypes: ApartmentTypeEntity[];

  @OneToMany(() => ProjectSpecEntity, projectSpec => projectSpec.project, { cascade: true, eager: true, nullable: true})
  specifications: ProjectAttachment[];
  
}
