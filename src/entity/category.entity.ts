import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { BaseEntity } from './base.entity';
import Category from '@/interfaces/category.interface';

@Entity({ name: 'category' })
export class CategoryEntity extends BaseEntity implements Category {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  userId: number;

  @Column()
  name: string;
  
}
