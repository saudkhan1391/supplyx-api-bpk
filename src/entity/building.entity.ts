import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Building } from '@/interfaces/building.interface';
import { ProjectEntity } from '@entity/project.entity';
import { FloorEntity } from './floor.entity';

@Entity({ name: 'building' })
export class BuildingEntity extends BaseEntity implements Building {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => ProjectEntity, { onDelete: "CASCADE", nullable: true })
  @JoinColumn({ name: 'projectId' })
  project: ProjectEntity;

  @Column()
  name: string;

  @Column()
  floorCount: string;

  @OneToMany(() => FloorEntity, floor => floor.building, { cascade: true, onDelete: "CASCADE", eager: true, nullable: true})
  floors: FloorEntity[];

}
