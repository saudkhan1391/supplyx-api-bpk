import { Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn, OneToMany, OneToOne } from 'typeorm';
import { User } from '@interfaces/users.interface';
import { BaseEntity } from './base.entity';
import { Attachment, CompanyAttachment, UserImage } from '@/interfaces/image.interface';
import { UserImageEntity } from './user-image.entity';

@Entity({ name: 'user' })
@Unique(['email'])
export class UserEntity extends BaseEntity implements User {
  bannerImage: CompanyAttachment[];
  
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;


  @Column('varchar', {nullable: true})
  companyName: string;

  @Column()
  lastName: string;

  @Column()
  firstName: string;

  @Column('varchar', {nullable: true})
  roleType: string;

  @Column('varchar',{nullable:true})
  country: string;

  @Column('varchar',{nullable:true})
  zipCode: number;

  @Column('varchar',{nullable:true})
  mobile: string;

  @Column('varchar',{nullable:true})
  phone: string;

  @Column('varchar',{nullable:true})
  phoneCode: string;

  @Column('varchar',{nullable:true})
  street: string;

  @Column('varchar',{nullable:true})
  state: string;

  @OneToMany(() => UserImageEntity, image => image.user,{ cascade: true, onDelete: 'CASCADE', eager: true, nullable: true })
  images: UserImage[];
}
