import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity({ name: 'catalog-product-mapping' })
export class CatalogProductMappingEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  catalogId: number;

  @Column()
  productId: number;
  
}
