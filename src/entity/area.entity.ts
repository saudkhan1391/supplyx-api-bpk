import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Area } from '@/interfaces/area.interface';
import { ApartmentTypeEntity } from '@entity/apartment-type.entity';

@Entity({ name: 'area' })
export class AreaEntity extends BaseEntity implements Area {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => ApartmentTypeEntity, { onDelete: 'CASCADE', nullable: true })
  @JoinColumn({ name: 'apartmentTypeId' })
  apartmentType: ApartmentTypeEntity;

  @Column()
  name: string;

  @Column()
  count: number;
}
