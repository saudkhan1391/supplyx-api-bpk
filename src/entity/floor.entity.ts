import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Floor } from '@interfaces/floor.interface';
import { BuildingEntity } from '@entity/building.entity';
import { FloorToApartmentEntity } from './floor-to-apartment.entity';

@Entity({ name: 'floor' })
export class FloorEntity extends BaseEntity implements Floor {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  floorNumber: number;

  @ManyToOne(() => BuildingEntity, { nullable: true })
  @JoinColumn({ name: 'buildingId' })
  building: BuildingEntity;

  @OneToMany(() => FloorToApartmentEntity, floorToApartment => floorToApartment.floor, { cascade: true, onDelete: "CASCADE", eager: true, nullable: true})
  apartments: FloorToApartmentEntity[];
}
