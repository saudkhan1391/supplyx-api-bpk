import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { AddressEntity } from './address.entity';
import { Project } from '@/interfaces/project.interface';

@Entity({ name: 'project' })
export class ProjectEntity extends BaseEntity implements Project {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  userId: number;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column()
  description: string;

  @OneToOne(() => AddressEntity)
  shippingAdress: AddressEntity


}
