import { Address } from '@/interfaces/address.interface';
import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, OneToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ProjectEntity } from './project.entity';

@Entity({ name: 'project-address' })
export class ProjectAddressEntity extends BaseEntity implements Address {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: true })
  street: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  state: string;

  @Column({ nullable: true })
  zipCode: string;

  @Column({ nullable: true })
  country: string;

}
