import { CompanyAttachment } from "@/interfaces/image.interface";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { CompanyEntity } from "./company.entity";

@Entity({ name: 'bannerimage' })
export class CompanyBannerEntity extends BaseEntity implements CompanyAttachment {
 
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => CompanyEntity, company => company.bannerImage)
  @JoinColumn({name: "companyId"})
  company: CompanyEntity;

  @Column('varchar')
  key: string;

  @Column('varchar')
  url: string
 
}