import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Apartment } from '@/interfaces/apartment.interface';
import { FloorEntity } from '@entity/floor.entity';

@Entity({ name: 'apartment' })
export class ApartmentEntity extends BaseEntity implements Apartment {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => FloorEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'floorId' })
  floor: FloorEntity;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column()
  unitCount: number;
}
