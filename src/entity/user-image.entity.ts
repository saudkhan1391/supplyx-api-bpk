import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Attachment, UserImage } from '@interfaces/image.interface';
import { UserEntity } from './users.entity';

@Entity({ name: 'user-image' })
export class UserImageEntity extends BaseEntity implements UserImage {
 
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => UserEntity, user => user.images)
  @JoinColumn({name: "userId"})
  user: UserEntity;

  @Column('varchar')
  key: string;

  @Column('varchar')
  url: string
 
}