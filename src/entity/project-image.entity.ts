import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ProjectImage } from '@interfaces/image.interface';

@Entity({ name: 'project-image' })
export class ProjectImageEntity extends BaseEntity implements ProjectImage {
 
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('varchar')
  key: string;

  @Column('varchar')
  url: string
 
}