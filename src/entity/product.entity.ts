import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Product } from '@interfaces/product.interface';
import { BaseEntity } from './base.entity';
import { CategoryEntity } from './category.entity';
import { ProductImageEntity } from './product-image.entity';
import { ProductSpecEntity } from './product-spec.entity';
import { CatalogEntity } from './catalog.entity';
import { Attachment } from '@/interfaces/image.interface';

@Entity({ name: 'product' })
export class ProductEntity extends BaseEntity implements Product {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  userId: number;

  @Column()
  productName: string;

  @Column({ nullable: true })
  brand: string;

  @Column({ nullable: true })
  manufacturer: string;

  @Column({ nullable: true })
  partNo: string;

  @Column({ nullable: true })
  hsCode: string;

  @Column('varchar', { nullable: true })
  description: string;

  @Column({ nullable: true })
  price: string;

  @Column({ nullable: true })
  minimumQuantity: number;

  @Column({ nullable: true })
  availableQuantity: number;

  @Column({ nullable: true })
  shipinfo: string;

  @OneToMany(() => ProductSpecEntity, spec => spec.product, { cascade: true, onDelete: 'CASCADE', eager: true, nullable: true })
  specifications: ProductSpecEntity[];

  @OneToMany(() => ProductImageEntity, image => image.product, { cascade: true, onDelete: 'CASCADE', eager: true, nullable: true })
  images: Attachment[];

  @Column()
  status: string;

  @ManyToOne(() => CategoryEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'categoryId' })
  category: CategoryEntity;

  @ManyToOne(() => CatalogEntity, { eager: true, nullable: true })
  @JoinColumn({ name: 'catalogId' })
  catalog: CatalogEntity;
}
