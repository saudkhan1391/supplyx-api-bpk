import { Apartment } from "@/interfaces/apartment.interface";
import { Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApartmentTypeEntity } from "./apartment-type.entity";
import { BaseEntity } from "./base.entity";
import { FloorEntity } from "./floor.entity";

@Entity({ name: 'floor-to-apartment-mapping' })
export class FloorToApartmentEntity extends BaseEntity implements Apartment {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => FloorEntity, { nullable: true })
  @JoinColumn({ name: 'floorId' })
  floor: FloorEntity

  @OneToOne(() => ApartmentTypeEntity, apartmentType => apartmentType.id, { eager: true, nullable: true})
  @JoinColumn({ name: 'apartmentTypeId' })
  apartmentType: ApartmentTypeEntity[];
  
}
