import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ApartmentType } from '@/interfaces/apartment-type.interface';
import { AreaEntity } from '@entity/area.entity';
import { ProjectEntity } from './project.entity';

@Entity({ name: 'apartmentType' })
export class ApartmentTypeEntity extends BaseEntity implements ApartmentType {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @ManyToOne(() => ProjectEntity, { onDelete: "CASCADE", nullable: true })
  @JoinColumn({ name: 'projectId' })
  project: ProjectEntity;

  @Column()
  name: string;

  @Column()
  size: number;

  @OneToMany(() => AreaEntity, (area) => area.apartmentType, { cascade: true, eager: true, nullable: true })
  areas: AreaEntity[]
}
